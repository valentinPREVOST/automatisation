##récupération de la lettre du disk usb
$var2 = Get-Volume -FriendlyName KINGSTON | Select -ExpandProperty DriveLetter

###################### PYTHON ######################
$pythonInstall = Start-Process ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\python\python-3.9.7-amd64.exe') -Wait



###################### FIREFOX ######################
$firefoxInstall = Start-Process ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\firefox\Firefox Setup 78.11.0esr.msi') -Wait

###################### IDE ######################
#$ideInstall = Start-Process ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\IDE\Notepad++\notepad++.exe') "/q" -Wait

###################### SELENIUM ######################
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\async_generator-1.10-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\attrs-21.2.0-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\certifi-2021.10.8-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\pycparser-2.20-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\cffi-1.15.0-cp39-cp39-win_amd64.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\cryptography-35.0.0-cp36-abi3-win_amd64.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\h11-0.12.0-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\idna-3.3-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\outcome-1.1.0-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\six-1.16.0-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\pyOpenSSL-21.0.0-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\sniffio-1.2.0-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\wsproto-1.0.0-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\sortedcontainers-2.4.0-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\trio-0.19.0-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\trio_websocket-0.9.2-py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\urllib3-1.26.7-py2.py3-none-any.whl')
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\selenium\selenium-4.0.0-py3-none-any.whl')


###################### PYNPUT ######################
pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\pynput\pynput-1.7.4-py2.py3-none-any.whl')


###################### OPENPYXL ######################
#pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\openpyxl\et_xmlfile-1.1.0-py3-none-any.whl')
#pip install ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\openpyxl\openpyxl-3.0.9-py2.py3-none-any.whl')


copy-Item ($var2+':\AUTOMATISATION_PROJET\AutomatisationUtils\geckodriver\geckodriver.exe') -Destination (Get-Childitem "C:\Users\$env:USERNAME\AppData\Local\Microsoft\WindowsApps\python.exe" | Select-Object -ExpandProperty Directory | Select -ExpandProperty fullname)
