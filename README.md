# AUTOMATISATION

Ce projet à pour but d'employer plusieurs outils afin d'automatiser des tâches. Nous utiliserons pour cela python et plusieurs librairies associées.

## SELENIUM WEBDRIVER

Selenium Webdriver permet d'intéragir avec le code d'une page web.

Lien : https://www.selenium.dev/documentation/webdriver/


## XPATH

Xpath permet de rechercher efficacement une élément web sur une page.

Lien : https://www.w3schools.com/xml/xpath_syntax.asp

Lien : https://www.scientecheasy.com/2019/08/xpath-axes.html/

Afin d'améliorer la robustesse dans notre code python, il est essentiel d'utiliser les fonctions XPATH.


## PREREQUIS

Pour l'instant, nous allons travailler uniquement dans un environnement Windows.

### PYTHON

Installer python 3.9.7

Lien : https://www.python.org/downloads/release/python-397/


### GECKODRIVER

Installer Geckodriver

Lien : https://github.com/mozilla/geckodriver/releases


### FIREFOX

Installer une version de Firefox de votre choix.

Lien : https://ftp.mozilla.org/pub/firefox/releases/


### LIBRAIRIE

selenium-4.0.0-py3-none-any
pynput-1.7.4-py2.py3-none-any

- Installation des modules
```
pip install selenium
pip install pynput
```
- Un script de déploiement est aussi disponible dans le respertoires scripts/installEnv. Ce script peut être util si vous ne disposez pas d'accès internet.

- Import des modules
```
import os
import time
from time import strftime
from datetime import datetime, timedelta
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from pynput import mouse, keyboard
from pynput.mouse import Button
from pynput.keyboard import Key
from selenium.webdriver.common.action_chains import ActionChains
import logging

```









