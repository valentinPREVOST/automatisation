# ASTUCES

## ActionChain

### >[1]<

- Lors de l'utilisation de la fonction move_to_element, il est important de faire pointer le curseur sur un autre élément une fois l'action terminé. C'est une pratique à faire car si nous souhaitons atteindre de nouveau un même élément avec move_to_element, il faut que le curseur pointe autrepart que l'élément. Sinon, nous ne pourrons pas cibler de nouveau l'élément car le curseur sera déja sur l'élément.




