# BONNES PRATIQUES

## Cliquer sur un élément

- voici les deux moyens de cliquer sur un élements web dans Python Webdriver : 

#### Moyen 1
- Utilisation du module ActionChains de Selenium

Lien : https://www.geeksforgeeks.org/action-chains-in-selenium-python/*

```
###permet de cibler un élément web
bouton_creation_equipement = driver.find_element(By.XPATH, lib_edgart.xpath_creer_equipement)
###permet d'initialiser la variable actions avec ActionsChains
actions = ActionChains(driver)
###permet de déplacer le curseur sur un élément
actions.move_to_element(bouton_creation_equipement
###permet de cliquer sur l'élément ciblé
actions.click(bouton_creation_equipement)
###permet de mettre à execution les commandes précédentes
actions.perform()
```

#### Moyen 2
- Utilisation d'un script javascript sous python.

Lien : https://dzone.com/articles/perform-actions-using-javascript-in-python-seleniu

```
###permet de cibler un élément web
programme = driver.find_element(By.XPATH, xpath_programme)
###permet de cliquer sur l'élément ciblé
driver.execute_script("arguments[0].click();", programme)
```

- Il est préférable dans la majorité des cas d'utiliser le moyen 2.

- Pourquoi? : Parfois un élément web peut ne pas être visible à l'écran, alors le moyen 1 échouera car le curseur ne pourra pas cibler l'élément non visible. Le moyen 2 n'a pas besoin de voir un élément web à l'écran pour cliquer dessus. 


## LOG

- Mise en place du système de log avec le module logging de python

Lien : https://docs.python.org/3/howto/logging.html



